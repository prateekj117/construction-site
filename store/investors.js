import { firebaseAction } from 'vuexfire'
import { firestore } from '@/plugins/vuexfirebase'

export const state = () => ({
  investors: [],
  investorRef: null,
  payload: [
    {
      name: state.name,
      info: state.info,
      imageUrl: state.imageUrl
    }
  ]
})

export const mutations = {
  setInvestorRef: (state, investorRef) => {
    state.investorRef = investorRef
  }
}

export const actions = {
  setInvestorRef: firebaseAction(
    ({ commit, bindFirebaseRef }, { ref, callbacks }) => {
      bindFirebaseRef('investors', ref, callbacks)
      commit('setInvestorRef', ref)
    }
  ),
  addInvestor: (_, payload) => {
    return firestore.collection('investors').add(payload)
  }
}
