import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

import firebaseconfig from '../firebaseconfig'

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseconfig)
  firebase.firestore().settings({
    timestampsInSnapshots: true
  })
}

const firestore = firebase.firestore()

export { firebase, firestore }
