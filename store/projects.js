import { firebaseAction } from 'vuexfire'
import { firestore } from '@/plugins/vuexfirebase'

export const state = () => ({
  projects: [],
  projectRef: null,
  payload: [
    {
      name: state.name,
      type: state.type,
      in_progress: state.in_progress,
      start_date: state.start_date,
      finish_date: state.finish_date,
      location: state.location,
      info: state.info,
      imageUrl: state.imageUrl
    }
  ]
})

export const mutations = {
  setProjectRef: (state, projectRef) => {
    state.projectRef = projectRef
  }
}

export const actions = {
  setProjectRef: firebaseAction(
    ({ commit, bindFirebaseRef }, { ref, callbacks }) => {
      bindFirebaseRef('projects', ref, callbacks)
      commit('setProjectRef', ref)
    }
  ),
  addProject: (_, payload) => {
    return firestore.collection('projects').add(payload)
  }
}
