import { firebaseAction } from 'vuexfire'

export const state = () => ({
  homeProjects: []
})

export const actions = {
  setHomeProjectsRef: firebaseAction(({ bindFirebaseRef }, { ref }) => {
    bindFirebaseRef('homeProjects', ref)
  })
}
