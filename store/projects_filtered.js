import { firebaseAction } from 'vuexfire'

export const state = () => ({
  projects_filtered: {}
})

export const actions = {
  setProjectsFilteredRef: firebaseAction(({ bindFirebaseRef }, { ref }) => {
    bindFirebaseRef('projects_filtered', ref)
  })
}
