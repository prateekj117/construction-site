import { firebaseMutations } from 'vuexfire'

export const strict = false

export const mutations = {
  ...firebaseMutations
}
