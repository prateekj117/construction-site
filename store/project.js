import { firebaseAction } from 'vuexfire'

export const state = () => ({
  project: {}
})

export const actions = {
  setProjectRef: firebaseAction(({ bindFirebaseRef }, { ref }) => {
    bindFirebaseRef('project', ref)
  })
}
